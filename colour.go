package pts

import (
	"github.com/fatih/color"
	"sync"
)

var colour colourHandler

type colourHandler struct {
	sync.RWMutex
	colors map[color.Attribute]*color.Color
}

func (c *colourHandler) get(attr color.Attribute) *color.Color {
	c.init(attr)

	c.RLock()
	defer c.RUnlock()

	return c.colors[attr]
}

func (c *colourHandler) init(attr color.Attribute) {
	c.Lock()
	defer c.Unlock()

	if c.colors == nil {
		c.colors = make(map[color.Attribute]*color.Color)
	}

	if cl, ok := c.colors[attr]; !ok {
		cl = color.New(attr)
		c.colors[attr] = cl
	}
}

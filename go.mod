module gitee.com/teval/pts

go 1.19

require (
	gitee.com/teval/curl v0.0.11
	gitee.com/teval/goz v1.0.7
	github.com/fatih/color v1.13.0
	github.com/rs/xid v1.3.0
	github.com/satori/go.uuid v1.2.0
)

require (
	github.com/basgys/goxml2json v1.1.0 // indirect
	github.com/idoubi/goutils v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/samber/lo v1.32.0 // indirect
	github.com/tidwall/gjson v1.9.1 // indirect
	github.com/tidwall/match v1.0.3 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/exp v0.0.0-20221010202428-3a778c567f61 // indirect
	golang.org/x/net v0.0.0-20210924151903-3ad01bbaa167 // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)

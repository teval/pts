package pts

import (
	"fmt"
	"gitee.com/teval/curl"
	"gitee.com/teval/goz"
)

func ExampleHttpWorks() {
	fmt.Println(HttpWorks(func() (stop bool, err error) {
		return false, NewErr("times")
	}))

	//Output:
	// times
}

func ExampleHttp() {
	b, err := Http(func() (res goz.ResponseBody, err error) {
		return curl.Get("http://baidu.com")
	})

	fmt.Println(b.GetContents(), err)

	//Output:
	// <html>
	// <meta http-equiv="refresh" content="0;url=http://www.baidu.com/">
	// </html>
	//  <nil>
}

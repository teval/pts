package pts

// Max 返回参数中的最大值（至少需要1个参数）
func Max[T Numeric | string](val T, vals ...T) T {
	for _, v := range vals {
		if val < v {
			val = v
		}
	}
	return val
}

// Min 返回参数中的最小值（至少需要1个参数）
func Min[T Numeric | string](val T, vals ...T) T {
	for _, v := range vals {
		if val > v {
			val = v
		}
	}
	return val
}

package pts

import (
	"fmt"
	"github.com/fatih/color"
)

func ExampleStrToFloat64() {
	f64 := StrToFloat64("5.2648")
	fmt.Println(fmt.Sprintf(`%v %T`, f64, f64))
	// Output:
	// 5.2648 float64
}

func ExampleStrToFloat32() {
	f32 := StrToFloat32("5.2648")
	fmt.Println(fmt.Sprintf(`%v %T`, f32, f32))
	// Output:
	// 5.2648 float32
}

func ExamplePrintColor() {
	PrintColor(color.FgHiMagenta, "abc", "def")
	fmt.Println("abc\tdef")
	// Output:
	// abc	def
}

func ExamplePrintRed() {
	PrintRed("error")
	// Output:
}

func ExampleJSON() {
	var s = struct {
		Name  string `json:"name"`
		Phone string `json:"phone"`
	}{
		"Teval", "15656565656",
	}
	fmt.Println(JSON(s))
	// Output:
	// {
	//     "name": "Teval",
	//     "phone": "15656565656"
	// }
}

func ExampleNewErr() {
	err := NewErr("假装有个错误")
	fmt.Println(fmt.Sprintf(`%v %T`, err, err))
	// Output:
	// 假装有个错误 *errors.errorString
}

func ExampleArrConcat() {
	arr := []int{1, 2, 3}
	arr2 := []int{4, 5, 6}
	fmt.Println(ArrConcat(arr, arr2))
	// Output:
	// [1 2 3 4 5 6]
}

func ExampleArrUnion() {
	arr := []int{1, 2, 3}
	arr2 := []int{2, 3, 4, 5, 6}
	fmt.Println(ArrUnion(arr, arr2))
	// Output:
	// [1 2 3 4 5 6]
}

func ExampleArrChunk() {
	fmt.Println(ArrChunk([]int{1, 2, 3, 4, 5, 6}))
	fmt.Println(ArrChunk([]int{1, 2, 3, 4, 5, 6}, 3))
	fmt.Println(ArrChunk([]int{1, 2, 3, 4, 5, 6}, 4))
	// Output:
	// [[1] [2] [3] [4] [5] [6]]
	// [[1 2 3] [4 5 6]]
	// [[1 2 3 4] [5 6]]
}

func ExampleArrIncludes() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrIncludes(arr, 0))
	fmt.Println(ArrIncludes(arr, 1))
	// Output:
	// false
	// true
}

func ExampleArrFill() {
	arr := []int{1, 2, 3, 4, 5}
	arr2 := ArrFill(&arr, 1)
	fmt.Println(arr)
	ArrFill(arr2, 2)
	fmt.Println(arr)
	// Output:
	// [1 1 1 1 1]
	// [2 2 2 2 2]

}

func ExampleArrFilter() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrFilter(arr, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// [2 4]
}

func ExampleArrMap() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrMap(arr, func(v, i int) int {
		return v * 2
	}))
	// Output:
	// [2 4 6 8 10]
}

func ExampleArrReduce() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrReduce(arr, func(prev string, curr int, idx int, arr []int) string {
		return fmt.Sprintf(`%s,%d`, prev, curr)
	}))

	fmt.Println(ArrReduce(arr, func(prev string, curr int, idx int, arr []int) string {
		return fmt.Sprintf(`%s,%d`, prev, curr)
	}, "start"))

	fmt.Println(ArrReduce(arr, func(prev []int, curr int, idx int, arr []int) []int {
		return append(prev, curr)
	}))

	// Output:
	// ,1,2,3,4,5
	// start,1,2,3,4,5
	// [1 2 3 4 5]
}

func ExampleArrUniq() {
	fmt.Println(ArrUniq([]int{1, 1, 2, 2, 3, 4, 4, 5}))

	fmt.Println(ArrUniq([]string{"a", "A", "B", "B", "B", "C"}))

	type s struct {
		A string
		B int
	}

	fmt.Println(ArrUniq([]s{{"x", 1}, {"X", 0}, {"X", 0}, {"x", 1}, {"x", 2}}))

	// Output:
	// [1 2 3 4 5]
	// [a A B C]
	// [{x 1} {X 0} {x 2}]
}

func ExampleArrSample() {
	fmt.Println(ArrSample([]int{1}))
	fmt.Println(ArrSample([]int{1, 1, 2, 2, 3, 4, 4, 5}))
	fmt.Println(ArrSample([]string{"a", "A", "B", "C"}, 5))

	// Output:
	// [1]
	// [3]
	// [C A B a]
}

func ExampleArrSampleOne() {
	fmt.Println(ArrSampleOne([]int{}))
	fmt.Println(ArrSampleOne([]int{1, 2, 3}))

	// Output:
	// 0
	// 1
}

func ExampleArrShuffle() {
	fmt.Println(ArrShuffle([]int{1, 2, 3, 4, 5}))
	// Output:
	// [3 2 5 1 4]
}

func ExampleArrCount() {
	fmt.Println(ArrCount([]int{0, 0, 0, 1, 1, 2, 3, 4, 5}))
	fmt.Println(ArrCount([]int{0, 0, 0, 1, 1, 2, 3, 4, 5}, 1))
	fmt.Println(ArrCount([]float64{1.625, 2.78, 3.521, 4.789, 5.666}, 3.521))
	// Output:
	// 3
	// 2
	// 1
}

func ExampleArrSum() {
	fmt.Println(ArrSum([]int{1, 2, 3, 4, 5}))
	fmt.Println(ArrSum([]float64{1.625, 2.78, 3.521, 4.789, 5.666}))
	// Output:
	// 15
	// 18.381
}

func ExampleArrMean() {
	fmt.Println(ArrMean([]int{1, 2, 3, 4, 5}))
	fmt.Println(ArrMean([]float64{1.625, 2.78, 3.521, 4.789, 5.666}))
	// Output:
	// 3
	// 3.6762
}

func ExampleArrProduct() {
	fmt.Println(ArrProduct([]int{1, 2, 3, 4, 5}))
	fmt.Println(ArrProduct([]float64{1.625, 2.78, 3.521, 4.789, 5.666}))
	// Output:
	// 120
	// 431.604131744695
}

func ExampleArrFind() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrFind(arr, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// 2
}

func ExampleArrFindIndex() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrFindIndex(arr, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// 1
}

func ExampleArrFindLast() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrFindLast(arr, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// 4
}

func ExampleArrFindLastIndex() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrFindLastIndex(arr, func(val int, idx int) bool {
		return val%2 == 0
	}))
	// Output:
	// 3
}

func ExampleArrSome() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrSome(arr, func(val int, idx int) bool {
		return val > 0
	}))
	fmt.Println(ArrSome(arr, func(val int, idx int) bool {
		return val > 5
	}))
	// Output:
	// true
	// false
}

func ExampleArrEvery() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrEvery(arr, func(val int, idx int) bool {
		return val > 0
	}))
	fmt.Println(ArrEvery(arr, func(val int, idx int) bool {
		return val > 1
	}))
	// Output:
	// true
	// false
}

func ExampleArrShift() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrShift(&arr))
	fmt.Println(arr)
	// Output:
	// 1
	// [2 3 4 5]
}

func ExampleArrUnShift() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrUnShift(&arr))
	fmt.Println(arr)
	fmt.Println(ArrUnShift(&arr, 0, -1))
	fmt.Println(arr)
	// Output:
	// 5
	// [1 2 3 4 5]
	// 7
	// [0 -1 1 2 3 4 5]
}

func ExampleArrPop() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrPop(&arr))
	fmt.Println(arr)
	// Output:
	// 5
	// [1 2 3 4]
}

func ExampleArrPush() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrPush(&arr))
	fmt.Println(arr)
	fmt.Println(ArrPush(&arr, 6, 7, 8))
	fmt.Println(arr)
	// Output:
	// 5
	// [1 2 3 4 5]
	// 8
	// [1 2 3 4 5 6 7 8]
}

func ExampleArrSplice() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrSplice(&arr, 2, 2, 7, 8, 9))
	fmt.Println(arr)
	fmt.Println(ArrSplice(&arr, -1, 2, 10, 11, 12))
	fmt.Println(arr)
	// Output:
	// [3 4]
	// [1 2 5 7 8 9]
	// [9]
	// [1 2 5 7 8 10 11 12]
}

func ExampleArrJoin() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(ArrJoin(arr, "x"))
	// Output:
	// 1x2x3x4x5
}

func ExampleJoin() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(Join(arr, "x"))
	// Output:
	// 1x2x3x4x5
}

func ExampleSpace() {
	fmt.Println(fmt.Sprintf(`_%s_`, Space(2)))
	fmt.Println(len(Space(2)))
	// Output:
	// _  _
	// 2
}

func ExampleStr() {
	fmt.Println(fmt.Sprintf(`%v %T`, Str(nil), Str(nil)))
	fmt.Println(fmt.Sprintf(`%v %T`, Str(5), Str(5)))
	// Output:
	// string
	// 5 string
}

func ExampleMax() {
	fmt.Println(Max(1, 2, 3, 4, 5, 4, 3, 2, 1))
	fmt.Println(Max("a", "b", "999"))
	fmt.Println(Max("18", "179"))
	fmt.Println(Max(1))
	// Output:
	// 5
	// b
	// 18
	// 1
}

func ExampleMin() {
	fmt.Println(Min(1, 2, 3, 4, 5, 4, 3, 2, 1))
	fmt.Println(Min("a", "b", "999"))
	fmt.Println(Min("18", "179"))
	fmt.Println(Min("1"))
	// Output:
	// 1
	// 999
	// 179
	// 1
}

func ExampleIIf() {
	v1 := IIf(true, 5, 1)
	fmt.Println(fmt.Sprintf(`%v %T`, v1, v1))

	v2 := IIf(false, "5", "1")
	fmt.Println(fmt.Sprintf(`%v %T`, v2, v2))
	// Output:
	// 5 int
	// 1 string
}

func ExampleDecode() {
	fmt.Println(Decode(5, 1, 10, 5, 50))
	fmt.Println(Decode(5, 1, 10, 3, 30))
	fmt.Println(Decode(5, 1, 10, 3, 30, 25))
	fmt.Println(Decode("5", "1", "10", "2", "20"))
	fmt.Println(Decode("5", "1", "10", "5", "20"))
	// Output:
	// 50
	// 0
	// 25
	//
	// 20
}

func ExampleNvl() {
	fmt.Println(Nvl(""))
	fmt.Println(Nvl("", "a"))
	fmt.Println(Nvl("", "", "b", "c"))
	fmt.Println(Nvl(0, 1, 2))
	fmt.Println(Nvl(5, 1, 2))
	// Output:
	//
	// a
	// b
	// 1
	// 5
}

func ExampleTrim() {
	fmt.Println(len(Trim(" 	\r\n	56 66  1")))
	cutset := "[\\s\\t\\r\\n1]+"
	fmt.Println(Trim(" 	\r\n	56 66  1", cutset))
	// Output:
	// 8
	// 56 66
}

func ExampleMd5() {
	fmt.Println(Md5([]byte("abcdefg")))
	// Output:
	// 7ac66c0f148de9519b8bd264312c4d64
}

func ExampleMd5Str() {
	fmt.Println(Md5Str("abcdefg"))
	// Output:
	// 7ac66c0f148de9519b8bd264312c4d64
}

func ExampleMd5Any() {
	var s = struct {
		Name  string
		Phone string
	}{
		"Teval", "15656565656",
	}
	fmt.Println(Md5Any(s))
	fmt.Println(Md5Any(fmt.Sprintf(`%+v`, s)))
	fmt.Println(fmt.Sprintf(`%+v`, s))
	// Output:
	// 29e1f5c14d500cdc87d7543e07acd9de
	// 29e1f5c14d500cdc87d7543e07acd9de
	// {Name:Teval Phone:15656565656}
}

func ExampleSha256() {
	fmt.Println(Sha256([]byte("abcdefg")))
	// Output:
	// 7d1a54127b222502f5b79b5fb0803061152a44f92b37e23c6527baf665d4da9a
}

func ExampleSha256Str() {
	fmt.Println(Sha256Str("abcdefg"))
	// Output:
	// 7d1a54127b222502f5b79b5fb0803061152a44f92b37e23c6527baf665d4da9a
}

func ExampleSha256Any() {
	var s = struct {
		Name  string
		Phone string
	}{
		"Teval", "15656565656",
	}
	fmt.Println(Sha256Any(s))
	fmt.Println(Sha256Any(fmt.Sprintf(`%+v`, s)))
	fmt.Println(fmt.Sprintf(`%+v`, s))
	// Output:
	// e0866e43fa7f387f59dac989699b73195e1b6b3d84e7e2ceb923c21c2b227e6b
	// e0866e43fa7f387f59dac989699b73195e1b6b3d84e7e2ceb923c21c2b227e6b
	// {Name:Teval Phone:15656565656}
}

func ExampleHmacSha256() {
	fmt.Println(HmacSha256([]byte("key"), []byte("abcdefg")))
	// Output:
	// 4718cebf3bde5716605b6fea813757e05dc234ee47bb6c81b7559d4731949771
}

func ExampleHmacSha256Str() {
	fmt.Println(HmacSha256Str("key", "abcdefg"))
	// Output:
	// 4718cebf3bde5716605b6fea813757e05dc234ee47bb6c81b7559d4731949771
}

func ExampleHmacSha256Any() {
	var s = struct {
		Name  string
		Phone string
	}{
		"Teval", "15656565656",
	}
	fmt.Println(HmacSha256Any([]byte("key"), s))
	fmt.Println(HmacSha256Any([]byte("key"), fmt.Sprintf(`%+v`, s)))
	fmt.Println(fmt.Sprintf(`%+v`, s))
	// Output:
	// c764cb7f3cebe567e98bc70b8a30f9a7b6d2d36112324abee4475fbd6756ef4e
	// c764cb7f3cebe567e98bc70b8a30f9a7b6d2d36112324abee4475fbd6756ef4e
	// {Name:Teval Phone:15656565656}
}

func ExampleUniqID() {
	fmt.Println(UniqID())
	// Output:
	// 2633020db8d74ce93f930ee98bc27fd3
}

func ExampleRandInt() {
	fmt.Println(RandInt(10, 11))
	// Output:
	// 10
}

func ExampleRandNum() {
	fmt.Println(RandNum(10))
	// Output:
	// 4087577164
}

func ExampleRandLocation() {
	fmt.Println(RandLocation())
	// Output:
	// 44.1285743781 100.3852417065
}

func ExampleRandStr() {
	fmt.Println(RandStr(32))
	// Output:
	// 62c7281i0gg5ejk54gcdmg3561793437
}

func ExampleRandKey() {
	fmt.Println(RandKey(32))
	// Output:
	// a6c0c22f130ac319bfa81f1a79de5c91
}

func ExampleRandChar() {
	fmt.Println(RandChar(24))
	// Output:
	// a6c0c22f130ac319bfa81f1a79de5c91
}

func ExampleRandIMEI() {
	fmt.Println(RandIMEI())
	// Output:
	// 254376154169509
}

func ExampleXID() {
	fmt.Println(XID())
	// Output:
	// c7282dogg5el2u4ikgvg
}

func ExampleUUID() {
	fmt.Println(UUID())
	// Output:
	// ea0935aa-4cbb-4bf3-8075-bfd81569baae
}

func ExampleRandDevice() {
	fmt.Println(RandDevice())
	// Output:
	// HUAWEI YAL-AL00
}

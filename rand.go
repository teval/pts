package pts

import (
	"github.com/rs/xid"
	mathRand "math/rand"
	"strconv"
	"strings"
	"time"
)

// RandInt 随机生成指定大小范围的整数
func RandInt(min, max int) int {
	if min == max {
		return min
	}

	time.Sleep(time.Nanosecond * 1)
	mathRand.Seed(time.Now().UnixNano())
	return min + mathRand.Intn(max+1-min)
}

func RandInt64(min, max int64) int64 {
	if min == max {
		return min
	}

	time.Sleep(time.Nanosecond * 1)
	mathRand.Seed(time.Now().UnixNano())
	return min + mathRand.Int63n(max+1-min)
}

// RandNum 随机生成指定长度的整数
func RandNum(length int) (str string) {
	for i := 0; i < length; i++ {
		str += Str(RandInt(0, 9))
	}
	return
}

// RandLocation 生成国内随机经纬度
func RandLocation() (lat float64, lon float64) {
	var (
		minLat int64 = 38600000000
		maxLat int64 = 535500000000
		minLon int64 = 736600000000
		maxLon int64 = 1350500000000
	)

	return float64(RandInt64(minLat, maxLat)) / 10000000000, float64(RandInt64(minLon, maxLon)) / 10000000000
}

// RandStr 生成指定长度的字符串
func RandStr(num int) string {
	return RandKey(num)
}

// RandKey 生成指定长度的字符串
func RandKey(num int) string {
	var str string
	for i := 0; i < num; i += 32 {
		str += Md5([]byte(xid.New().String()))
	}
	return str[0:num]
}

func RandChar(num int) (chars string) {
	str := `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`

	for i := 0; i < Nvl(num, 0); i++ {
		chars += string(str[RandInt(0, 61)])
	}

	return
}

// RandIMEI 生成随机的手机IMEI串号
func RandIMEI() string {
	str := Str(1000000+RandInt(0, 8999999)) + Str(1000000+RandInt(0, 8999999))
	arr := strings.Split(str, "")

	a, b := 0, 0
	for i, l := 0, len(arr); i < l; i++ {
		m, _ := strconv.Atoi(arr[i])
		if i%2 == 0 {
			a += m
		} else {
			n := m * 2
			b += n/10 + n%10
		}
	}

	r := (a + b) % 10
	if r != 0 {
		r = 10 - r
	}

	return str + Str(r)
}

func RandDevice() (brand, model string) {
	var (
		i = 0
		m = RandInt(0, len(DeviceList)-1)
	)

	for brand, models := range DeviceList {
		if i >= m {
			switch l := len(models); l {
			case 0:
			case 1:
				return brand, models[0]
			default:
				return brand, models[RandInt(0, l-1)]
			}
		}
		i++
	}

	return "", ""
}

var DeviceList = map[string][]string{
	"HUAWEI": {
		"LIO-AN00", // Mate 30 Pro 5G
		"TAS-AN00", // Mate 30 5G
		"TAS-AL00", // Mate 30
		"LYA-AL10", // Mate 20 Pro
		"HMA-AL00", // Mate 20
		"VOG-AL00", // P30 Pro
		"ELE-AL00", // P30
		"OXF-AN10", // Honor V30 Pro
		"OXF-AN00", // Honor V30
		"PCT-AL10", // Honor V20
		"YAL-AL10", // Honor 20 Pro
		"YAL-AL00", // Honor 20
		"WLZ-AN00", // Nova 6
		"SEA-AL10", // Nova 5 Pro
	},
	"XiaoMi": {
		"MI9", //
	},
	"Meizu": {
		"M973Q", // 16s Pro

	},
	"OPPO": {
		"PCLM10", // Reno Ace
		"PCRT00", // Reno3 Pro
	},
	"vivo": {
		"V1916A",  // IQOO PRo
		"V1824A",  // IQOO
		"V1936A",  // IQOO Neo
		"V1923A",  // Nex 3
		"V1938CT", // X30
		"V1938T",  // X30 Pro
	},
	"samsung": {
		"SM-N9760", // Note 10+
		"SM-N9700", // Note 10
		"SM-G9750", // S10+
		"SM-G9730", // S10
	},
	"OnePlus": {
		"HD1910", // 7T Pro
		"HD1900", // 7T
		"GM1910", // 7 Pro
		"GM1900", // 7
	},
	"realme": {
		"RMX1931", // x20 Pro
	},
	"nubia": {
		"NX627J", // z20
		"NX629J", // HongMo 3
	},
	"smartisan": {
		"DT1901A", // 3
	},
}

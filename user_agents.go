package pts

import (
	"encoding/json"
	"github.com/samber/lo"
	"strings"
)

type DeviceInfo struct {
	AppName    string `json:"appName"`
	Connection struct {
		Downlink      float64 `json:"downlink"`
		EffectiveType string  `json:"effectiveType"`
		Rtt           int     `json:"rtt"`
	} `json:"connection"`
	Platform       string  `json:"platform"`
	PluginsLength  int     `json:"pluginsLength"`
	Vendor         string  `json:"vendor"`
	UserAgent      string  `json:"userAgent"`
	ViewportHeight int     `json:"viewportHeight"`
	ViewportWidth  int     `json:"viewportWidth"`
	DeviceCategory string  `json:"deviceCategory"`
	ScreenHeight   int     `json:"screenHeight"`
	ScreenWidth    int     `json:"screenWidth"`
	Weight         float64 `json:"weight"`
}

var Devices []DeviceInfo

func init() {
	if err := json.Unmarshal(deviceListBytes, &Devices); err != nil {
		panic(err)
	}
}

func RandomDeviceInfo() DeviceInfo {
	return lo.Sample(Devices)
}

func RandomAndroidDeviceInfo() DeviceInfo {
	return lo.Sample(lo.Filter(Devices, func(v DeviceInfo, i int) bool {
		return v.DeviceCategory == "mobile" && strings.Contains(strings.ToLower(v.UserAgent), "android")
	}))
}

func RandomIphoneDeviceInfo() DeviceInfo {
	return lo.Sample(lo.Filter(Devices, func(v DeviceInfo, i int) bool {
		return v.DeviceCategory == "mobile" && strings.Contains(strings.ToLower(v.UserAgent), "iphone")
	}))
}

func RandomDesktopDeviceInfo() DeviceInfo {
	return lo.Sample(lo.Filter(Devices, func(v DeviceInfo, i int) bool {
		return v.DeviceCategory == "desktop"
	}))
}

func RandomUserAgent() string {
	device := lo.Sample(Devices)
	return device.UserAgent
}

func RandomAndroidUserAgent() string {
	device := lo.Sample(lo.Filter(Devices, func(v DeviceInfo, i int) bool {
		return v.DeviceCategory == "mobile" && strings.Contains(strings.ToLower(v.UserAgent), "android")
	}))
	return device.UserAgent
}

func RandomIphoneUserAgent() string {
	device := lo.Sample(lo.Filter(Devices, func(v DeviceInfo, i int) bool {
		return v.DeviceCategory == "mobile" && strings.Contains(strings.ToLower(v.UserAgent), "iphone")
	}))
	return device.UserAgent
}

func RandomDesktopUserAgent() string {
	device := lo.Sample(lo.Filter(Devices, func(v DeviceInfo, i int) bool {
		return v.DeviceCategory == "desktop"
	}))
	return device.UserAgent
}
